# Example 0 - Help Guide

In this example, we demonstrate how to access help information using `tadah` binary. Ensure Tadah! is installed and accessible through your `$PATH`. If not, use the full path to the Tadah! binary in your commands.

## Accessing Tadah! Help

To display a list of subcommands and their descriptions, use:

```bash
tadah --help
```

or

```bash
tadah -h
```

## Subcommand Help

For help with specific subcommands, such as `train`, use:

```bash
tadah train --help
```

## Runtime Errors

Tadah! provides useful runtime errors, such as when a configuration file is missing a necessary key. While we strive for reliability, no code is entirely bug-free. If you encounter issues, please report them on Tadah!'s GitLab repository.

.. _contact:

Contribute to Tadah!
====================

We welcome contributions to Tadah!, an open-source project.

Getting Help
------------

If you need assistance or have questions, feel free to contact us:

- **Email**: marcin.kirsz@ed.ac.uk 

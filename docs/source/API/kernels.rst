Kernels
=======

Kern_Linear
...........

.. doxygenclass:: Kern_Linear
    :members:

Kern_Quadratic
..............

.. doxygenclass:: Kern_Quadratic
    :members:

Kern_LQ
.......

.. doxygenclass:: Kern_LQ
    :members:

Kern_RBF
........

.. doxygenclass:: Kern_RBF
    :members:

Kern_Polynomial
...............

.. doxygenclass:: Kern_Polynomial
    :members:

Kern_Sigmoid
............

.. doxygenclass:: Kern_Sigmoid
    :members:

Kernel
......

.. doxygenclass:: Kern_Base
    :members:

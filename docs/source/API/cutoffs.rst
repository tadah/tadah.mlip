Cutoffs
=======

.. doxygenclass:: Cut_Base
    :members:

.. doxygenclass:: Cut_Dummy
    :members:

.. doxygenclass:: Cut_Cos
    :members:

.. doxygenclass:: Cut_Tanh
    :members:

.. doxygenclass:: Cut_Poly2
    :members:

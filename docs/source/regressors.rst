Regressors
==========

M_KRR
-----

.. doxygenclass:: M_KRR

Kernels
.......

Kern_Linear
~~~~~~~~~~~

.. doxygenclass:: Kern_Linear

Kern_LQ
~~~~~~~

.. doxygenclass:: Kern_LQ

Kern_Polynomial
~~~~~~~~~~~~~~~

.. doxygenclass:: Kern_Polynomial

Kern_Quadratic
~~~~~~~~~~~~~~

.. doxygenclass:: Kern_Quadratic

Kern_RBF
~~~~~~~~

.. doxygenclass:: Kern_RBF

Kern_Sigmoid
~~~~~~~~~~~~

.. doxygenclass:: Kern_Sigmoid

M_BLR
-----

.. doxygenclass:: M_BLR

Basis Functions
...............

BF_Linear
~~~~~~~~~

.. doxygenstruct:: BF_Linear

BF_Polynomial2
~~~~~~~~~~~~~~

.. doxygenstruct:: BF_Polynomial2

'''
This script generates documentation in rst format from
the TOML dictionary.
'''
import pytomlpp as p
import os
import shutil
import subprocess

def download_toml_file(repo_url, file_path, output_path):
    try:
        # Clone the repository into a temporary directory
        repo_name = "temp_repo"
        subprocess.run(['git', 'clone', '--depth', '1', repo_url, repo_name], check=True)

        # Copy the desired file to the output path
        full_file_path = os.path.join(repo_name, file_path)
        if os.path.exists(full_file_path):
            shutil.copy(full_file_path, output_path)
            print(f"Successfully downloaded {file_path} to {output_path}")
        else:
            print(f"File {file_path} not found in the repository.")

    except subprocess.CalledProcessError as e:
        print(f"Error handling the repository: {e}")
    finally:
        # Clean up
        if os.path.exists(repo_name):
            shutil.rmtree(repo_name)

download_toml_file(
    'https://git.ecdf.ed.ac.uk/tadah/core.git',
    'config/config_keys.toml',
    'config/config_keys.toml'
)

# Path to TOML dict
# d = p.load("../src/configs/config_keys.toml",'r')
d = p.load("config/config_keys.toml",'r')

# Path to rst outfile
# f = open("source/config/config_keys.rst", "w")
f = open("config/config_keys.rst", "w")

header='''
.. _ConfigSection:

Configuration File
==================

This section describes the format of the configuration file used by Tadah!.

The configuration file controls the training process, specifying one or more datasets for use during the training stage. It defines cutoff functions and corresponding radii along with the regression model and descriptor choices.

Tadah! supports two- and many-body descriptors, allowing for separate descriptors with their corresponding cutoff radii.

Key/Value Pairs
---------------

The primary structure in a configuration file is the KEY/VALUE pair. Each KEY/VALUE pair must be on a separate line, with the KEY appearing first. The KEY is always a string, followed by its VALUE. The format and type of a VALUE depend on the specific KEY.

Common Usage
------------

Typically, only a subset of KEYS is needed to train a model. Tadah! will use default values for some keys. An error will occur if a required KEY with no default value is missing:

.. code-block:: bash

   [user@host:~] $ tadah train -c config.train
   terminate called after throwing an instance of 'std::runtime_error'
     what():  Key not found: DBFILE
   Aborted (core dumped)

This message indicates that the :ref:`DBFILE` KEY was not specified in the :file:`config.train` file. To resolve this, add the :ref:`DBFILE` key and its corresponding value to :file:`config.train`.

Key Specifics
-------------

The meaning of some KEYS may change based on the model or descriptor in use. Refer to :ref:`SupportedKEYS` for each model or descriptor to understand the required KEYS and their explanations.

Comments
--------

Use the `#` symbol to add comments in the configuration file.

Multiple Values
---------------

Some KEYS can have multiple values, specified in one of two ways:

- Single line:

  .. code-block:: bash

     KEY VALUE1 VALUE2 VALUE3

- Multiple lines:

  .. code-block:: bash

     KEY VALUE1
     KEY VALUE2
     KEY VALUE3

Value Limits
------------

If the number of values exceeds the maximum allowed, an error will occur. For example, specifying :ref:`RCTYPE2B` twice when only one value is allowed will result in:

.. code-block:: bash

   [user@host:~] $ tadah train -c config.train
   terminate called after throwing an instance of 'std::runtime_error'
     what():  Repeated key RCTYPE2B Cut_Cos
   Aborted (core dumped)

.. _SupportedKEYS

Supported KEYS:
---------------

This section contains all KEYS currently used by Tadah!.
\n
'''
f.write(header)

for key,val in d.items():
    f.write(".. _%s:\n\n" % key)    # create reference
    f.write("%s\n" % key)
    f.write("%s\n" % (len(key)*"."))
    if val['value_format']!='' and val['value_type']!='':
        f.write("%s" % (key))
        for vtype,vfmt in zip(val['value_type'],val['value_format']):
            f.write(" [*%s*] %s" % (vtype,vfmt))
        f.write("\n")
    if len(val['value_N'])!=0:
        f.write("   *Max number of values*:")
        for valN in val['value_N']:
            f.write("  %s" % abs(valN))
        f.write("\n\n")
    if len(val['default'])!=0:
        f.write("   *Default*:")
        for valN in val['default']:
            f.write("  %s" % (str(valN).lower()
                              if type(valN) is bool else valN))
        f.write("\n\n")
    if len(val['description'])!=0:
        f.write("   *Description*:\n\n")
        f.write("   %s\n\n" % val['description'])
    if len(val['examples'])!=0:
        for i,example in enumerate(val['examples']):
            f.write("   *Example*: %d ::\n\n" % (i+1))
            f.write("       %s\n\n" % example)

    # Deal with suboptions
    for key2,val2 in val.items():
        if type(val2) is dict:
            if val2['value_format']!='' and val2['value_type']!='':
                f.write("%s" % (key))
                for vtype,vfmt in zip(val2['value_type'],val2['value_format']):
                    f.write(" [*%s*] %s" % (vtype,vfmt))
                f.write("\n")
            if len(val2['value_N'])!=0:
                f.write("   *Max number of values*: ")
                for valN in val2['value_N']:
                    f.write("  %s" % abs(valN))
                f.write("\n\n")
            if len(val2['description'])!=0:
                f.write("   *Description*:\n\n")
                f.write("   %s\n\n" % val2['description'])
            if len(val2['examples'])!=0:
                for i,example in enumerate(val2['examples']):
                    f.write("   *Example*: %d ::\n\n" % (i+1))
                    f.write("       %s\n\n" % example)

f.close()

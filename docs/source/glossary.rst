.. _glossary:

Glossary
========

.. glossary::
    
    AED
        Atomic Energy Descriptor

    FD
        Force Descriptor
        
        Negative of the derivative of the AED wrt to the atom position.


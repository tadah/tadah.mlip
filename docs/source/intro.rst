.. _introduction:

Introduction
============

Tadah! is a fast and modular machine learning **software** and C++ **library** specifically for interatomic potential development. It’s built in modern C++ to provide an easy-to-use, modular, and extensible toolkit.

Tadah! offers a LAMMPS interface compatible with all descriptors and models. It can be used as command-line software for training models or using machine learning potentials for prediction, or as an advanced C++ library.

Key Features
------------

- **Hyperparameter Optimizer**: Enhances potential transferability.
- **LAMMPS Integration**: Fully interfaced.
- **Community-Driven**: Open to new ideas and implementations.
- **Speed**: Fast model development cycles reduce wait times.
- **Regular Updates**: Frequent new features and optimizations.
- **Open-Source**: Publicly accessible and modifiable.
- **Modular Design**: Mix and match descriptors for flexible testing.
- **Extensible**: Easy addition of new descriptors.
- **Desktop and MPI Versions**: Suitable for various computational needs.

.. _obtaining_tadah:

Obtaining Tadah!
----------------

The Tadah! codebase is divided into several interacting modules. There are two user-facing modules: Tadah!MLIP, which provides a set of tools for the development of MLIPs, and Tadah!LAMMPS, which allows deployment of trained models using molecular dynamics with LAMMPS.

The code repository is available at:

    https://git.ecdf.ed.ac.uk/tadah


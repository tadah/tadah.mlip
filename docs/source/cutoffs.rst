.. _cutoffs:

Cutoff Functions
================

Cutoff functions that can be used with either two- or many-body descriptors.

Cut_Cos
.......

.. doxygenclass:: Cut_Cos

Cut_CosS
.......

.. doxygenclass:: Cut_CosS

Cut_Dummy
.........

.. doxygenclass:: Cut_Dummy

Cut_Poly
.........

.. doxygenclass:: Cut_Poly

Cut_PolyS
.........

.. doxygenclass:: Cut_PolyS

Cut_PT
.........

.. doxygenclass:: Cut_PT

Cut_Tanh
........

.. doxygenclass:: Cut_Tanh


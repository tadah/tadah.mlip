.. _descriptors:

Descriptors
===========

Two-Body
--------
Below is a list of all two-body descriptors supported by Tadah:

D2_LJ
.....

.. doxygenclass:: D2_LJ

D2_BP
.....

.. doxygenclass:: D2_BP

D2_Blip
.......

.. doxygenclass:: D2_Blip

D2_EAM
......

.. doxygenclass:: D2_EAM

D2_MIE
......

.. doxygenclass:: D2_MIE

D2_ZBL
........

.. doxygenclass:: D2_ZBL

D2_Dummy
........

.. doxygenclass:: D2_Dummy

D2_mJoin
........

.. doxygenclass:: D2_mJoin


Many-Body
----------
This section lists all many-body type descriptors supported by Tadah! Some can calculate non-spherical charge distributions, thus overcoming the limitations of the Embedded Atom Method:

DM_Blip
.......

.. doxygenclass:: DM_Blip

DM_EAD
......

.. doxygenclass:: DM_EAD

DM_EAM
......

.. doxygenclass:: DM_EAM

DM_mEAD
.......

.. doxygenclass:: DM_mEAD

DM_mEAD functions
~~~~~~~~~~~~~~~~~

.. doxygenclass:: F_RLR

DM_Dummy
........

.. doxygenclass:: DM_Dummy

DM_mJoin
........

.. doxygenclass:: DM_mJoin
